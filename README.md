# Sudoku Solver

To launch the solver, run `main.py`.

I wrote this many years ago, so the code is much more complicated (and
probably slower) than it needs to be.
