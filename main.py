# Sudoku Solver
# =============

# Display loading screen ASAP
import pygame

WIDTH = 800
HEIGHT = 600
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
FONTNAME = None

pygame.init()
S = pygame.display.set_mode((WIDTH, HEIGHT))

FONTFL = pygame.font.Font(FONTNAME, 90)
LOADING = FONTFL.render('Loading...', 1, BLACK)


def GetCentreBlit(surface, width):
    return (width - surface.get_width()) / 2

S.fill(WHITE)
S.blit(LOADING, (GetCentreBlit(LOADING, WIDTH), 100))
pygame.display.update()
pygame.display.set_caption('Sudoku Solver')

# Main program
import json
import random
from pygame.locals import *
import time

RED = (255, 0, 0)

class Cell:
    def __init__(self):
        self.colours = {'put': BLACK,
                        'unk': (100, 100, 100),
                        'got': (0, 0, 200),
                        'imp': RED,
                        'ges': (30, 100, 50),
                        }

        self.canbe = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.ctype = 'unk'
        self.value = 0
        self.colour = self.colours[self.ctype]

        self.oldcanbe = []
        self.oldctype = ''
        self.oldvalue = -1

        self.haschanged = True

        self.image = pygame.Surface((0, 0))

        self.oldfonts = [None, None]
        self.oldsize = None
        self.lcol = None

    def Set(self, value, ctype='put'):
        '''Set the cell's value to a known'''
        if value != 0:
            self.value = value
            self.ctype = ctype
            self.colour = self.colours[self.ctype]
            self.canbe = []
        else:
            self.__init__()

    def GetVal(self):
        '''Get the cell's value'''
        if self.ctype == 'unk':
            return self.canbe
        else:
            return self.value

    def Remove(self, things):
        '''Remove all things in things from canbe'''
        for i in things:
            if i in self.canbe:
                self.canbe.remove(i)

    def HasChanged(self):
        self.haschanged = True
        neededs = (self.canbe == self.oldcanbe,
                   self.ctype == self.oldctype,
                   self.value == self.oldvalue)
        if all(neededs):
            self.haschanged = False
        self.oldcanbe = self.canbe.copy()
        self.oldctype = self.ctype[:]
        self.oldvalue = self.value + 0
        return self.haschanged

    def Render(self, size, bfont, sfont):
        '''Render a sudoku cell'''
        canvas = pygame.Surface((size, size))
        canvas.fill((255, 255, 255))
        neededs = (self.oldfonts != [bfont, sfont],
                   size != self.oldsize,
                   self.colour != self.lcol)
        if any(neededs):
            self.tbs = [None]
            for i in range(1, 10, 1):
                self.tbs.append(sfont.render(str(i), 1, self.colour))

            self.bigs = [None]
            for i in range(1, 10, 1):
                self.bigs.append(bfont.render(str(i), 1, self.colour))

        if self.ctype in ('put', 'got', 'imp', 'ges'):
            blitloc = (GetCentreBlit(self.bigs[self.value], size), 0)
            canvas.blit(self.bigs[self.value], blitloc)
            self.image = canvas.convert()

        elif self.ctype == 'unk':
            if self.canbe == []:
                canvas.fill(RED)
                return canvas
            a = size // 3
            cellblitlocs = [(0, 0), (a, 0), (2 * a, 0),
                            (0, a), (a, a), (2 * a, a),
                            (0, 2 * a), (a, 2 * a), (2 * a, 2 * a),
                            ]

            for number in range(1, 10, 1):
                if number in self.canbe:
                    nwidth = self.tbs[number].get_width()
                    nheight = self.tbs[number].get_height()
                    canvas.blit(self.tbs[number], ((cellblitlocs[number - 1][0] + (a - nwidth) // 2), (cellblitlocs[number - 1][1] + (a - nheight) // 2)))
            self.image = canvas.convert()

        else:
            raise Exception('Not a valid type: ' + str(type(cell)))

        self.oldfonts = [bfont, sfont].copy()
        self.oldsize = size + 0
        self.lcol = self.colour


class button():
    def __init__(self, col, size, loc, text, fsize=36, textcol=WHITE, align='c', pad=5, states=None, font=None, state=0):
        self.col = col
        self.location = loc
        self.size = size
        self.text = text
        self.textcol = textcol
        self.fontsize = fsize
        self.font = font
        self.align = align
        self.value = None
        self.ext = ''

        self.statesinfo = states
        if states:
            self.statecount = len(self.statesinfo[0][1])
            self.state = state
            for part in self.statesinfo:
                if part[0] == 'Text colour':
                    self.textcol = part[1][self.state]
                elif part[0] == 'Value':
                    self.value = part[1][self.state]
                elif part[0] == 'Text+':
                    self.ext = part[1][self.state]
                elif part[0] == 'Colour':
                    self.textcol += part[1][self.state]

        self.image = pygame.surface.Surface((size[0], size[1]))
        self.image.fill(self.col)
        f = pygame.font.Font(self.font, self.fontsize)
        texti = f.render(self.text + self.ext, 1, self.textcol)
        h = texti.get_height()
        w = texti.get_width()
        if self.align == 'c':
            blitpos = ((self.size[0] - w) / 2, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)
        elif self.align == 'r':
            blitpos = ((self.size[0] - w) - pad, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)
        else:
            blitpos = (pad, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)

    def isdown(self, mp):
        x = self.location[0]
        y = self.location[1]
        if (x < mp[0] < (x + self.size[0])) and (y < mp[1] < (y + self.size[1])):
            return True
        return False

    def changestate(self):
        if self.statesinfo is None:
            return

        self.state += 1
        if self.state >= len(self.statesinfo[0][1]):
            self.state = 0

        for part in self.statesinfo:
            if part[0] == 'Text colour':
                self.textcol = part[1][self.state]
            elif part[0] == 'Value':
                self.value = part[1][self.state]
            elif part[0] == 'Text+':
                self.ext = part[1][self.state]
            elif part[0] == 'Colour':
                self.textcol += part[1][self.state]

        self.image = pygame.surface.Surface((self.size[0], self.size[1]))
        self.image.fill(self.col)
        f = pygame.font.Font(self.font, self.fontsize)
        texti = f.render(self.text + self.ext, 1, self.textcol)
        h = texti.get_height()
        w = texti.get_width()
        if self.align == 'c':
            blitpos = ((self.size[0] - w) / 2, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)
        elif self.align == 'r':
            blitpos = ((self.size[0] - w) - pad, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)
        else:
            blitpos = (pad, (self.size[1] - h) / 2)
            self.image.blit(texti, blitpos)
        return self.value


def GetXY(cellnum, size):
    '''Get XY position from cellnum'''
    x = round((cellnum % 9) * (size / 9)) + 4
    y = round((cellnum // 9) * (size / 9)) + 4
    return (x, y)


def GetCellnum(pos, size):
    '''Get Cellnum from XY position'''
    rx = int((pos[0] / size) * 9)
    ry = int((pos[1] / size) * 9)
    return (ry * 9) + rx


def GetAll(thing, lst):
    return [i for i, val in enumerate(lst) if val == thing]

cages = [[0, 1, 2, 3, 4, 5, 6, 7, 8],
         [9, 10, 11, 12, 13, 14, 15, 16, 17],
         [18, 19, 20, 21, 22, 23, 24, 25, 26],
         [27, 28, 29, 30, 31, 32, 33, 34, 35],
         [36, 37, 38, 39, 40, 41, 42, 43, 44],
         [45, 46, 47, 48, 49, 50, 51, 52, 53],
         [54, 55, 56, 57, 58, 59, 60, 61, 62],
         [63, 64, 65, 66, 67, 68, 69, 70, 71],
         [72, 73, 74, 75, 76, 77, 78, 79, 80],

         [0, 9, 18, 27, 36, 45, 54, 63, 72],
         [1, 10, 19, 28, 37, 46, 55, 64, 73],
         [2, 11, 20, 29, 38, 47, 56, 65, 74],
         [3, 12, 21, 30, 39, 48, 57, 66, 75],
         [4, 13, 22, 31, 40, 49, 58, 67, 76],
         [5, 14, 23, 32, 41, 50, 59, 68, 77],
         [6, 15, 24, 33, 42, 51, 60, 69, 78],
         [7, 16, 25, 34, 43, 52, 61, 70, 79],
         [8, 17, 26, 35, 44, 53, 62, 71, 80],

         [0, 1, 2, 9, 10, 11, 18, 19, 20],
         [3, 4, 5, 12, 13, 14, 21, 22, 23],
         [6, 7, 8, 15, 16, 17, 24, 25, 26],
         [27, 28, 29, 36, 37, 38, 45, 46, 47],
         [30, 31, 32, 39, 40, 41, 48, 49, 50],
         [33, 34, 35, 42, 43, 44, 51, 52, 53],
         [54, 55, 56, 63, 64, 65, 72, 73, 74],
         [57, 58, 59, 66, 67, 68, 75, 76, 77],
         [60, 61, 62, 69, 70, 71, 78, 79, 80],
         ]

ErrorImage = [[4], [], [4], [], [], [], [], [], [],
              [4], [4], [4], [4], [], [], [], [], [],
              [], [], [4], [], [], [], [], [], [],
              [], [], [], [], range(0, 10), [], [], [], [],
              [], [], [], range(0, 10), [], range(0, 10), [], [], [],
              [], [], [], [], range(0, 10), [], [], [], [],
              [], [], [], [], [], [4], [], [4], [],
              [], [], [], [], [], [4], [4], [4], [4],
              [], [], [], [], [], [], [], [4], [],
              ]

FONTB = pygame.font.Font(FONTNAME, 72)
FONTS = pygame.font.Font(FONTNAME, 24)

CELLSIZE = WIDTH // 9 - 28

NUMKEYS = {K_0: 0,
           K_1: 1,
           K_2: 2,
           K_3: 3,
           K_4: 4,
           K_5: 5,
           K_6: 6,
           K_7: 7,
           K_8: 8,
           K_9: 9,
           K_DELETE: 0,
           }

BS = (160, 35)
X = 620
buttons = [button(RED, BS, (X, 45), 'Solve', states=[['Value', [False, True]], ['Text colour', [(70, 70, 70), WHITE]]], state=0, font=FONTNAME),
           button(RED, BS, (X, 90), 'Fast Solve', state=0, font=FONTNAME),
           button(RED, BS, (X, 135), 'Pause', states=[['Value', [False, True]], ['Text colour', [(70, 70, 70), WHITE]]], state=0, font=FONTNAME),
           button(RED, BS, (X, 180), 'Speed: ', states=[['Value', [1, 3, 6, 12, 20, 999]], ['Text+', ['1', '3', '6', '12', '20', 'max']]], state=5, font=FONTNAME),
           button(RED, BS, (X, 225), 'Strength: ', states=[['Value', [0, 1, 2, 3, 4, 5]], ['Text+', ['0', '1', '2', '3', '4', '5']]], state=0, font=FONTNAME),
           button(RED, BS, (X, 270), 'Guessing', states=[['Value', [False, True]], ['Text colour', [(70, 70, 70), WHITE]]], state=0, font=FONTNAME),
           button(RED, BS, (X, 315), 'Clear', font=FONTNAME),
           button(RED, BS, (X, 360), 'Restart', font=FONTNAME),
           button(RED, BS, (X, 405), 'Save', font=FONTNAME),
           button(RED, BS, (X, 450), 'Load', font=FONTNAME),
           button(RED, BS, (X, 495), 'Solidify', font=FONTNAME),
           button(RED, BS, (X, 540), 'Quit', font=FONTNAME),
           ]

FONTSFFPS = []
for dfps in range(0, 2000, 1):
    FONTSFFPS.append(FONTS.render('FPS: ' + str(dfps), 1, (140, 140, 140)))

clock = pygame.time.Clock()
grid = []
for i in range(0, 81, 1):
    grid.append(Cell())

backgrid = pygame.Surface((600, 600))
backgrid.fill(WHITE)
for x in range(0, 10, 1):
    if x % 3 == 0:
        w = 3
    else:
        w = 1
    x = round(x * (200 / 3))
    pygame.draw.line(backgrid, BLACK, (x, 0), (x, 600), w)

for y in range(0, 10, 1):
    if y % 3 == 0:
        w = 3
    else:
        w = 1
    y = round(y * (200 / 3))
    pygame.draw.line(backgrid, BLACK, (0, y), (600, y), w)

backgrid.convert()

stage = 0
maxstage = 0
MAXMAXSTAGE = 5
fps = 999

nxtinpt = -1
notselected = 1
nochange = False
nochangecount = 0
solve = False
canguess = False
guessing = False
finished = False
fastsolve = False
drawwholething = True
DDA = False
ext = False
while not ext:
    # Get input
    buttonpressed = None
    for event in pygame.event.get():
        if event.type == MOUSEBUTTONUP:
            if fastsolve:
                fastsolve = False
                drawwholething = True
                break
            pos = pygame.mouse.get_pos()
            if pos[0] < HEIGHT and pos[1] < HEIGHT:
                cn = GetCellnum(pos, HEIGHT)
                if cn == nxtinpt:
                    notselected = 1
                    nxtinpt = -1
                else:
                    nxtinpt = cn
                    notselected = False

            # Button presses
            else:
                for ibut in buttons:
                    if ibut.isdown(pos):
                        buttonpressed = ibut.text
                        newvalue = ibut.changestate()
                        break

        elif event.type == KEYDOWN:
            if fastsolve:
                fastsolve = False
                drawwholething = True
                break
            elif not notselected:
                if event.key in list(NUMKEYS.keys()):
                    orig = grid[nxtinpt]
                    # Get number changes
                    if orig.ctype in ('put', 'got', 'imp'):
                        orig = orig.GetVal()

                    grid[nxtinpt].Set(NUMKEYS[event.key])
                    # Refresh the grid
                    if event.key == K_DELETE or NUMKEYS[event.key] != orig:
                        for cell in range(0, len(grid), 1):
                            if grid[cell].ctype in ('unk', 'got', 'ges'):
                                grid[cell].__init__()

                            elif grid[cell].ctype == 'imp':
                                grid[cell].Set(grid[cell].GetVal(), 'put')
                        guessing = False
                        nochangecount = 0
                        nochange = False
                        finished = False

                    stage = 0

                # Move the square selector around with the arrow keys
                elif event.key == K_UP:
                    nxtinpt = max(((nxtinpt // 9) - 1, 0)) * 9 + nxtinpt % 9

                elif event.key == K_DOWN:
                    nxtinpt = min(((nxtinpt // 9) + 1, 8)) * 9 + nxtinpt % 9

                elif event.key == K_LEFT:
                    nxtinpt = max(((nxtinpt % 9) - 1, 0)) + (nxtinpt // 9) * 9

                elif event.key == K_RIGHT:
                    nxtinpt = min(((nxtinpt % 9) + 1, 8)) + (nxtinpt // 9) * 9

                elif event.key == K_ESCAPE:
                    notselected = 1
                    nxtinpt = -1

        elif event.type == QUIT:
            ext = True
            break

    if buttonpressed == 'Solve':
        solve = not not newvalue

    elif buttonpressed == 'Fast Solve':
        if not solve:
            buttons[0].changestate()
        solve = True
        fastsolve = True
        nochangecount = 0
        nochange = False

        for cell in grid:
            if cell.ctype in ('unk', 'got', 'ges'):
                cell.__init__()
        stage = 0
        guessing = False
        finished = False
        st = time.time()

    elif buttonpressed == 'Pause':
        DDA = newvalue
        if not DDA:
            stage = 0
            guessing = False
            nochangecount = 0
            nochange = False
            finished = False

    elif buttonpressed == 'Speed: ':
        fps = newvalue

    elif buttonpressed == 'Strength: ':
        maxstage = newvalue

    elif buttonpressed == 'Guessing':
        canguess = not not newvalue

        if not canguess:
            guessing = False
            for cell in grid:
                if cell.ctype in ('ges', 'unk'):
                    cell.__init__()
            stage = 0
            guessing = False
            nochangecount = 0
            nochange = False
            finished = False

    elif buttonpressed == 'Clear':
        for cell in grid:
            cell.__init__()
        stage = 0
        guessing = False
        nochangecount = 0
        nochange = False
        finished = False

    elif buttonpressed == 'Restart':
        for cell in grid:
            if cell.ctype in ('unk', 'got', 'ges'):
                cell.__init__()
        stage = 0
        guessing = False
        nochangecount = 0
        nochange = False
        finished = False

    elif buttonpressed == 'Save':
        output = [
            cell.canbe if cell.ctype in ("got", "ges") else cell.GetVal()
            for cell in grid
        ]
        with open(".savefile", "w") as f:
            json.dump(output, f)

    elif buttonpressed == 'Load':
        try:
            with open(".savefile") as f:
                p = json.load(f)
            for cellnum in range(0, len(grid), 1):
                grid[cellnum].__init__()
                if type(p[cellnum]) == int:
                    grid[cellnum].Set(p[cellnum])
                else:
                    grid[cellnum].canbe = p[cellnum]

        except FileNotFoundError:
            for cell in range(0, len(grid), 1):
                grid[cell].canbe = ErrorImage[cell]
        stage = 0
        guessing = False
        nochangecount = 0
        nochange = False
        finished = False
        for cell in grid:
            if cell.ctype == 'unk':
                cell.__init__()

    elif buttonpressed == 'Solidify':
        for cell in grid:
            if cell.ctype != 'unk':
                cell.Set(cell.GetVal())

    elif buttonpressed == 'Quit':
        ext = True
        break

    # Solve the actual thing
    stop = finished or DDA
    for cg in range(0, len(cages), 1):
        if stop:
            break
        cage = cages[cg]

        # Get canbe values
        if stage == 0:
            ins = []
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype != 'unk':
                    ins.append(grid[cage[t]].GetVal())

            for t in range(0, len(cage), 1):
                grid[cage[t]].Remove(ins)

            # Find duplicates in cages
            alreds = []
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype in ('put', 'imp'):
                    v = grid[cage[t]].GetVal()
                    if v in alreds:
                        grid[cage[t]].Set(v, ctype='imp')
                        grid[cage[alreds.index(v)]].Set(v, ctype='imp')

                    else:
                        alreds.append(v)
                else:
                    alreds.append(0)

        # Only one thing (Naked single) & guessing
        elif stage == 1 and solve:
            # Naked single
            if not (nochange and canguess):
                for t in range(0, len(cage), 1):
                    cid = cage[t]  # The cell's id
                    if grid[cid].ctype == 'unk' and len(grid[cid].canbe) == 1:
                        if not guessing:
                            grid[cage[t]].Set(grid[cid].GetVal()[0], ctype='got')
                        else:
                            grid[cage[t]].Set(grid[cid].GetVal()[0], ctype='ges')
                        stage = -1
                        stop = True
                        break

            # Had to guess twice: maybe restart
            elif guessing and random.randint(0, 1):
                for cell in grid:
                    if cell.ctype in ('ges', 'unk'):
                        cell.__init__()
                nochangecount = 0
                stop = True
                stage = -1
                guessing = False
                break

            # Guessing
            else:
                # Find most reduced cells 'cos these are gonna be guessed right
                possibles = []
                for cell in grid:
                    possibles.append(len(cell.canbe))

                # Zeroes are cells that have already been found
                while 0 in possibles:
                    possibles[possibles.index(0)] = 99

                lw = -1
                i = 1
                while lw == -1:
                    i += 1
                    lw = -1
                    try:
                        if not random.randint(0, 6):
                            lw = possibles.index(i)
                        else:
                            possibles[possibles.index(i)] = 99
                            i -= 1
                    except ValueError:
                        pass

                if i >= 8:
                    continue
                nc = random.choice(grid[lw].canbe)
                grid[lw].Set(nc, 'ges')

                asump = [lw, nc]

                guessing = True
                nochangecount = 0
                stop = True
                stage = -1

        # One in cage (Hidden single)
        elif stage == 2:
            quants = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            # Count the number of each digit
            for num in range(1, 10, 1):
                for t in range(0, len(cage), 1):
                    if num in grid[cage[t]].canbe:
                        quants[num] += 1

            try:
                make = quants.index(1)

            except ValueError:
                continue

            # Actually make it
            for t in range(0, len(cage), 1):
                cid = cage[t]  # The cell's id
                if make in grid[cid].canbe:
                    if solve:
                        if not guessing:
                            grid[cid].Set(make, ctype='got')
                        else:
                            grid[cid].Set(make, ctype='ges')
                        stage = -1
                        stop = True
                        break
                    else:
                        grid[cid].canbe = [make]

        # N in cage swappy (Hidden pair/triple/quad...)
        elif stage == 3:
            for t in range(0, len(cage), 1):
                cid = cage[t]  # The cell's id
                nextt = False
                # Matches is how many matches for each digit
                matches = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                matchones = [[], [], [], [], [], [], [], [], [], []]
                if len(grid[cid].canbe) < 3:
                    continue
                # Find how many of each there are in the rest
                for tcanbe in grid[cid].canbe:
                    for rest in range(0, len(cage), 1):
                        if rest == t:
                            continue  # Same one twice
                        if tcanbe in grid[cage[rest]].canbe:
                            matches[tcanbe] += 1
                            matchones[tcanbe].append(rest)

                # Go through and identify pairs
                n = 1
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if matchones[match] in catcher:
                            alls = GetAll(matchones[match], catcher)
                            result = [numcatcher[alls[0]], match]
                            grid[cage[t]].canbe = result
                            nextt = True
                            break
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

                if nextt:
                    continue

                # Triplets
                n = 2
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if catcher.count(matchones[match]) == n:
                            alls = GetAll(matchones[match], catcher)
                            result = [numcatcher[alls[0]], numcatcher[alls[1]], match]
                            grid[cage[t]].canbe = result
                            nextt = True
                            break
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

                if nextt:
                    continue

                # Quadruplets
                n = 3
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if catcher.count(matchones[match]) == n:
                            alls = GetAll(matchones[match], catcher)
                            grid[cage[t]].canbe = [numcatcher[alls[0]], numcatcher[alls[1]], numcatcher[alls[2]], match]
                            nextt = True
                            break
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

                if nextt:
                    continue

                # Quintuplets
                n = 4
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if catcher.count(matchones[match]) == n:
                            alls = GetAll(matchones[match], catcher)
                            grid[cage[t]].canbe = [numcatcher[alls[0]], numcatcher[alls[1]], numcatcher[alls[2]], numcatcher[alls[3]], match]
                            nextt = True
                            break
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

                if nextt:
                    continue

                # Sextuplets
                n = 5
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if catcher.count(matchones[match]) == n:
                            alls = GetAll(matchones[match], catcher)
                            grid[cage[t]].canbe = [numcatcher[alls[0]], numcatcher[alls[1]], numcatcher[alls[2]], numcatcher[alls[3]], numcatcher[alls[4]], match]
                            nextt = True
                            break
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

                if nextt:
                    continue

                # Hepteplets
                n = 6
                catcher = []
                numcatcher = []
                for match in range(1, len(matches), 1):
                    if matches[match] == n:
                        if catcher.count(matchones[match]) == n:
                            alls = GetAll(matchones[match], catcher)
                            grid[cage[t]].canbe = [numcatcher[alls[0]], numcatcher[alls[1]], numcatcher[alls[2]], numcatcher[alls[3]], numcatcher[alls[4]], numcatcher[alls[5]], match]
                        else:
                            catcher.append(matchones[match])
                            numcatcher.append(match)

        # Naked pair
        elif stage == 4:
            # Go through all pairs of cells
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype != 'unk':
                    continue
                for u in range(t + 1, len(cage), 1):
                    if grid[cage[u]].ctype != 'unk':
                        continue

                    # Determine if they are length two and equal
                    if grid[cage[t]].canbe == grid[cage[u]].canbe and len(grid[cage[t]].canbe) == 2:
                        # Remove their values from other cells in their cage
                        for i in range(0, len(cage), 1):
                            if i not in (t, u):
                                grid[cage[i]].Remove(grid[cage[t]].canbe)
        # Naked n
        elif stage == 5:
            # Naked triples

            # Go through all threes of cells
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype != 'unk':
                    continue
                for u in range(t + 1, len(cage), 1):
                    if grid[cage[u]].ctype != 'unk':
                        continue
                    for v in range(u + 1, len(cage), 1):
                        if grid[cage[v]].ctype != 'unk':
                            continue

                        # Find triples that are the right size
                        numsin = set(grid[cage[t]].canbe + grid[cage[u]].canbe + grid[cage[v]].canbe)
                        if len(numsin) == 3:
                            # Remove from cage
                            for i in range(0, len(cage), 1):
                                if i not in (t, u, v):
                                    grid[cage[i]].Remove(numsin)
                        # Find impossible arrangements
                        elif len(numsin) < 3:
                            grid[cage[t]].canbe = []
                            grid[cage[u]].canbe = []
                            grid[cage[v]].canbe = []

            # Naked quad
            # Go through all quads of numbers
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype != 'unk':
                    continue
                for u in range(t + 1, len(cage), 1):
                    if grid[cage[u]].ctype != 'unk':
                        continue
                    for v in range(u + 1, len(cage), 1):
                        if grid[cage[v]].ctype != 'unk':
                            continue
                        for w in range(v + 1, len(cage), 1):
                            if grid[cage[w]].ctype != 'unk':
                                continue

                            # Find fours that are the right size
                            numsin = set(grid[cage[t]].canbe + grid[cage[u]].canbe + grid[cage[v]].canbe + grid[cage[w]].canbe)
                            if len(numsin) == 4:
                                # Remove from cage
                                for i in range(0, len(cage), 1):
                                    if i not in (t, u, v, w):
                                        grid[cage[i]].Remove(numsin)
                            # Find impossible arrangements
                            elif len(numsin) < 4:
                                grid[cage[t]].canbe = []
                                grid[cage[u]].canbe = []
                                grid[cage[v]].canbe = []
                                grid[cage[w]].canbe = []

            # Naked quint
            for t in range(0, len(cage), 1):
                if grid[cage[t]].ctype != 'unk':
                    continue
                for u in range(t + 1, len(cage), 1):
                    if grid[cage[u]].ctype != 'unk':
                        continue
                    for v in range(u + 1, len(cage), 1):
                        if grid[cage[v]].ctype != 'unk':
                            continue
                        for w in range(v + 1, len(cage), 1):
                            if grid[cage[w]].ctype != 'unk':
                                continue
                            for x in range(w + 1, len(cage), 1):
                                if grid[cage[x]].ctype != 'unk':
                                    continue

                                # Find fives that are the right size
                                numsin = set(grid[cage[t]].canbe + grid[cage[u]].canbe + grid[cage[v]].canbe + grid[cage[w]].canbe + grid[cage[x]].canbe)
                                if len(numsin) == 5:
                                    # Remove from cage
                                    for i in range(0, len(cage), 1):
                                        if i not in (t, u, v, w, x):
                                            grid[cage[i]].Remove(numsin)
                                # Find impossible arrangements
                                elif len(numsin) < 5:
                                    grid[cage[t]].canbe = []
                                    grid[cage[u]].canbe = []
                                    grid[cage[v]].canbe = []
                                    grid[cage[w]].canbe = []
                                    grid[cage[x]].canbe = []

    stage += 1
    if stage > maxstage:
        stage = 0

    # Draw the background
    if not fastsolve:
        S.fill(WHITE)
        S.blit(backgrid, (0, 0))

    # Draw the grid
    changed_this_time = False
    for cellnum in range(0, len(grid), 1):
        # Only render cells that have changed, to save time
        if grid[cellnum].HasChanged() or drawwholething:
            if not fastsolve:
                # Render the cell
                grid[cellnum].Render(CELLSIZE, FONTB, FONTS)
            changed_this_time = True

        if not fastsolve:
            # Draw the cell to the screen
            S.blit(grid[cellnum].image, GetXY(cellnum, HEIGHT))

    drawwholething = False
    if not notselected:
        # Draw the box around a selected cell
        pygame.draw.rect(S, (80, 160, 100), GetXY(nxtinpt, HEIGHT) + tuple([CELLSIZE] * 2), 3)

    finished = False
    if not changed_this_time:
        nochangecount += 1
    else:
        nochangecount = 0

    if nochangecount > maxstage:
        nochange = True
        finished = True
        for cell in grid:
            if cell.ctype == 'unk':
                finished = False
        if canguess:
            stage = 1
        if finished or not canguess:
            if fastsolve:
                fastsolve = False
                S.blit(FONTB.render(str(time.time() - st), 1, (10, 10, 10)), (700, 10))
            drawwholething = True

    else:
        nochange = False

    # Draw the buttons
    if not fastsolve:
        for ibut in buttons:
            S.blit(ibut.image, ibut.location)

    # FPS-meter
    clock.tick(fps)
    
    if not fastsolve:
        S.blit(FONTSFFPS[round(clock.get_fps())], (730, 580))
        pygame.display.update()
